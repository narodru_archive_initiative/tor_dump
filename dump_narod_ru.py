#!/usr/bin/python3
from stem import Signal
from stem.control import Controller
from time import sleep
import queue
import requests
import threading
import json

TOR_PROXY = {'http': 'socks5h://127.0.0.1:9050'}
THREAD_NUM = 16
LOGFILE = "narod_ru.json"
HOST_IP = "http://193.109.247.10"


def dump(in_q, out_q, proxies, controller):
    try:
        s = requests.Session()
        host = in_q.get()
        while True:
            try:
                r = s.get('http://%s' % host, proxies=proxies)
                in_q.task_done()
                info = {'host': host, 'status_code': r.status_code, 'text': r.status_code == 200 and r.text or ''}
                out_q.put_nowait(info)
            except Exception as e:
                print(str(e))
                s = requests.Session()
                controller.signal(Signal.NEWNYM) # change ip
                sleep(10)
                continue # retry attempt
            host = in_q.get()
    except queue.Empty:
        return


def logger(q):
    outfile = open('narod_ru.json', 'w')
    while True:
        try:
            item = q.get()
            if item['status_code'] not in [404,]:
                print(item['host'], item['status_code'])
            print(json.dumps(item), file=outfile)
            outfile.flush()
            q.task_done()
        except queue.Empty:
            outfile.close()
            return


if __name__ == "__main__":
    infile = open('narod_ru.txt', 'r')
    hostlist = [x.strip() for x in infile]
    infile.close()
    in_queue = queue.Queue()
    out_queue = queue.Queue()
    controller = Controller.from_port(port=9051)
    controller.authenticate(password="<censored>")
    for hostname in hostlist:
        in_queue.put_nowait(hostname)
    threads = []
    for i in range(THREAD_NUM):
        t = threading.Thread(target=dump, args=(in_queue, out_queue, TOR_PROXY, controller))
        threads.append(t)
        t.start()
    logger = threading.Thread(target=logger, args=(out_queue,))
    logger.start()
    in_queue.join()
    for i in threads:
        t.join()
    logger.join()
